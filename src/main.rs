use qdrant_client::client::QdrantClient;
use qdrant_client::qdrant::{vectors_config::Config, VectorParams, VectorsConfig, SearchPoints, CreateCollection, Distance, PointStruct};
use serde_json::json;
use anyhow::Result;


#[tokio::main]
async fn main() -> Result<()> {
    // Initialize the client
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Create a collection
    client
    .create_collection(&CreateCollection {
        collection_name: "my_collection".to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Dot.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    })
    .await?;

    // Add vectors
let points = vec![
    PointStruct::new(
        1,
        vec![0.05, 0.61, 0.76, 0.74],
        json!(
            {"city": "Berlin"}
        )
        .try_into()
        .unwrap(),
    ),
    PointStruct::new(
        2,
        vec![0.19, 0.81, 0.75, 0.11],
        json!(
            {"city": "London"}
        )
        .try_into()
        .unwrap(),
    ),
    PointStruct::new(
        3, // 注意更新ID
        vec![0.12, 0.01, 0.85, 0.71],
        json!(
            {"city": "Tokyo"}
        )
        .try_into()
        .unwrap(),
    ),
    PointStruct::new(
        4, // 注意更新ID
        vec![0.27, 0.91, 0.15, 0.31],
        json!(
            {"city": "Hong Kong"}
        )
        .try_into()
        .unwrap(),
    ),
    // 新增加的点
    PointStruct::new(
        5, // 新点的ID
        vec![0.33, 0.22, 0.44, 0.55],
        json!(
            {"city": "New York"}
        )
        .try_into()
        .unwrap(),
    ),
    PointStruct::new(
        6, // 新点的ID
        vec![0.88, 0.44, 0.66, 0.77],
        json!(
            {"city": "Sydney"}
        )
        .try_into()
        .unwrap(),
    ),
];

    let operation_info = client
        .upsert_points_blocking("my_collection".to_string(), None, points, None)
        .await?;

    dbg!(operation_info);

    // Run a query
    let search_result = client
    .search_points(&SearchPoints {
        collection_name: "my_collection".to_string(),
        vector: vec![0.8, 0.2, 0.6, 0.4], 
        filter: None,
        limit: 2, 
        with_payload: Some(true.into()),
        ..Default::default()
    })
    .await?;

    // print the result
    for point in search_result.result {
        println!("Point: {:?}", point);
    }

    Ok(())
}

# Week7-mini-proj

> Bruce Xu  
> zx112  

# Demo

this is the terminal output after running `cargo run`
![](/demo.png)

this is the website output using this url

`http://localhost:6333/dashboard#/collections/my_collection/visualize`

![](/website.png)

# Steps

0. Initialize `Git project` and create new cargo project on local
1. add following dependencies in `.toml` file
   1. `anyhow = "1.0.81"
qdrant-client = "1.8.0"
serde_json = "1.0.114"
tokio = { version = "1.36.0", features = ["rt-multi-thread"] }
tonic = "0.11.0"
`
2. Edit the `main.rs` file by learning from  `https://qdrant.tech/documentation/quick-start/`
3. run the following command to pull from `docker`
   1. `docker run -p 6333:6333 -p 6334:6334 \    
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant`

    you should see following 
    ![](/docker.png)
4. Meanwhile,  run `cargo run` to see the output


# Explanation on code

1. **Initialize the client**
2. **Create a collection**: change the collection name into your own
3. **Add vectors**: add your own dataset for the program
4. **Run a query**: define your own query and you can change any variable values as you want
5. **Print the output**